package com.bubble.bee.security;

public class SecurityConstants {
    public static final String SECRET = "Mtrkj8734MfgT1@uejd";
    public static final long EXPIRATION_TIME = 3600;
    public static final String TOKEN_PREFIX = "Bearer";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/users/signup";
}
