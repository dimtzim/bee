package com.bubble.bee.controller;

import com.bubble.bee.domain.User;
import com.bubble.bee.repository.UserRepository;
import com.bubble.bee.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;
    private UserRepository userRepository;
//    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<User> getUsers(){
        return userService.getUserList();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public User getSingleUser(@PathVariable Integer id){
        return userService.getUserWithId(id);
    }

    @PostMapping("/signup")
    public String signup(@RequestBody User user){
        return userService.signup(user);
    }

}
